<?php
/*
Template Name: Test pagination
*/

echo '<h1>Test</h1>';

$paged = (get_query_var('page')) ? get_query_var('page') : 1;

$args = array(
	'post_type' 	 => array( 'post' ),
	'posts_per_page' => 3,
	'paged' => $paged );

$query = new WP_Query( $args );

if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
	<h2><?php the_title() ?></h2> 
<?php endwhile;
endif;

wp_reset_postdata();

// echo paginate_links( array(
//   'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
//   'total'        => $query->max_num_pages,
//   'current'      => max( 1, get_query_var( 'paged' ) ),
//   'format'       => '?paged=%#%',
//   'show_all'     => true,
//   'type'         => 'plain',
//   'end_size'     => 3,
//   'mid_size'     => 1,
//   'prev_next'    => true,
//   'prev_text'    => '',
//   'next_text'    => '',
//   'add_args'     => false,
//   'add_fragment' => '', ) );

echo paginate_links( array(
    'base'    => get_permalink() . '%#%' . '/',
    'format'  => '',
    'end_size'     => 1,
    'mid_size'     => 3,
    'current' => $paged,
    'total'   => $query->max_num_pages,
    'prev_text'    => ('<<'),
    'next_text'    => ('>>'),
) );

// echo paginate_links( [
//   'base'    => user_trailingslashit( wp_normalize_path( get_permalink() .'/%#%/' ) ),
//   'format'       => '',
// 	'current' => max( 1, get_query_var( 'page' ) ),
// 	'total'   => $query->max_num_pages,
// ] );