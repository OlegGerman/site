<?php 

add_shortcode( 'phone', 'view_phone' );

function view_phone( $atts, $content ){

	$atts = shortcode_atts( [
		'name'   => 'Name',
		'number' => '11111111',
	], $atts, 'testfilter');

	return 'Custom content: ' . $content . ' My name: ' . $atts['name'] . ', my phone is: ' . $atts['number'];
}

add_filter('shortcode_atts_testfilter', 'func_testfilter', 10, 4 );

function func_testfilter( $out, $pairs, $atts, $shortcode ){
	$out['name'] = 'Artem';
	return $out;
}