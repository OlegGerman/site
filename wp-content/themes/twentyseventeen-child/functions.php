<?php 
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles(){
	wp_enqueue_style( 'twentyseventeen-style', get_template_directory_uri() . '/style.css', array() );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('twentyseventeen-style') );
}

add_action( 'wp_enqueue_scripts', 'twentyseventeen_scripts' );