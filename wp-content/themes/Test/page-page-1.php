<?php get_header(); ?>

<p class="test">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum doloremque ipsam rerum ea repellat quia sint fugit laudantium expedita itaque.</p>

<div class="wrap" style='position: relative; z-index: 1;'>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<div class="main-content">
				<?php
					$query = new WP_Query( array(
						'post_type' => array( 'studies' ),
						'posts_per_page' => 1,
					) );
					if ( $query->have_posts() ) :
						while ( $query->have_posts() ) :
							$query->the_post(); ?>
							<div><a href="<?php the_permalink(); ?>"><?php the_title('<h1>', '</h1>'); ?></a></div>
						<?php endwhile;
					else :
						echo 'Posts not found';
					endif;
					wp_reset_postdata();

					if ( $query->max_num_pages > 1 ) : ?>
						<script>
							var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
							var posts = '<?php echo serialize($query->query_vars); ?>';
							var current_page = <?php echo ( get_query_var('paged') ) ? get_query_var('paged') : 1; ?>;
							var max_pages = '<?php echo $query->max_num_pages; ?>';
						</script>
						<div id="loadmore"><?php _e('Download', 'textdomain'); ?></div>
					<?php endif;
				?>
			</div>

		</main>
	</div>	
</div>
<?php
get_footer();
