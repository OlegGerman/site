( function( $ ) {
	//Update site link color in real time...
	wp.customize( 'test', function( value ) {
		value.bind( function( newval ) {
			$('p.test').css('color', newval );
		} );
	} );
	
} )( jQuery );