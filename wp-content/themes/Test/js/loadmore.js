jQuery(function($){
	$('#loadmore').on('click', (function(){
		var data = {
			'action': 'loadmore',
			'query': posts,
			'current_page': current_page
		};
		$.ajax({
			url: ajaxurl,
			data: data, 
			type: 'POST',
			success:function(data){
				if ( data ) {
					$('#loadmore').before(data);
					current_page++;
					if (current_page == max_pages) $("#loadmore").remove();
				} else {
					$('#loadmore').remove();
				}
			}
		});
	}));
});

