(function($) {

  $('.burger-button').click(function() {
    $('.wrapper-burger-menu').toggleClass("active-respon-menu"); // устанавливает отн. позиционирование у элемента обвертки
    $('.wrapper-burger-menu > nav').toggleClass("act"); // выдвигает меню в лево от -1000px до 25px
    $('body').toggleClass('lock');
  });

  $(".wrapper-burger-menu nav ul li.menu-item-has-children > a").click(function(event){
    event.preventDefault(); // запрет перехода по ссылке главного пункта меню
  });

  if (window.matchMedia("(max-width: 950px)").matches) {
    $('.wrapper-burger-menu nav ul > li.menu-item-has-children').click(function(event) {
      $(this).toggleClass('active-respon-menu-sub-menu');
      if($(this).hasClass('active-respon-menu-sub-menu')){
        $(this).children('ul').delay(200).fadeIn(300); /* скорость появления пункта меню */
      } else {
        $(this).children('ul').delay(200).fadeOut(200); /* скорость исчезновения пункта меню */
      }
    });
  } else {
    var hoverTimeout;
    $('.wrapper-burger-menu nav ul > li.menu-item-has-children').hover(
      function(){
        var _this = $(this);
        hoverTimeout = setTimeout(function () {
        _this.children('ul').fadeIn(300); /* скорость появления пункта меню */
        }, 200);
      },
      function(){
        $(this).children('ul').fadeOut(500); /* скорость исчезновения пункта меню */
        clearTimeout(hoverTimeout);
      });
  }

})( jQuery );