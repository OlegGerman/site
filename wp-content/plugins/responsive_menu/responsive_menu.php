<?php
/*
Plugin Name: Responsive menu
Description: Responsive super menu
*/
add_action( 'after_setup_theme', 'theme_register_nav_menu' );
function theme_register_nav_menu() {
  register_nav_menu( 'Super_responsive_menu', 'Super responsive menu' );
}

add_action( 'wp_enqueue_scripts', 'theme_add_scripts' );
function theme_add_scripts() {
   wp_enqueue_style( 'rsm-style', plugins_url('responsive_menu/assets/css/style.css', dirname(__FILE__) ), array(), time(), false );
   wp_enqueue_script( 'rsm-script', plugins_url('/responsive_menu/assets/js/custom.js', dirname(__FILE__) ), array('jquery'), time(), true );
}

add_filter( 'wp_nav_menu', 'change_html_wp_nav_menu', 10, 2 );
function change_html_wp_nav_menu( $nav_menu, $args ){
  if ( $args->theme_location === 'Super_responsive_menu' ){
  return '<div class="wrapper-burger-menu">
            <div class="burger-menu">
              <div class="burger-button">
                <span class="burger-span"></span>
              </div>
            </div>'
            . $nav_menu .
          '</div>';
  } else { return $nav_menu; }
}

function view_responsive_super_menu() {
  if ( has_nav_menu( 'Super_responsive_menu' ) ){
    wp_nav_menu( array(
      'theme_location'  => 'Super_responsive_menu',
      'container' => 'nav',
      'container_class' => 'super_responsive_menu_nav',
      'depth' => 2,
      ) );
    }
}