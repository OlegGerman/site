<?php
/*
Plugin Name: Vertical menu
Description: Vertical super menu
*/
add_action( 'after_setup_theme', 'vm_register_nav_menu' );
function vm_register_nav_menu() {
  register_nav_menu( 'Vertical_menu', 'Vertical menu' );
}

add_action( 'wp_enqueue_scripts', 'vm_add_scripts' );
function vm_add_scripts() {
   wp_enqueue_style( 'vm-style', plugins_url('vertical_menu/assets/css/style.css', dirname(__FILE__) ), array(), time(), false );
   wp_enqueue_script( 'vm-script', plugins_url('/vertical_menu/assets/js/custom.js', dirname(__FILE__) ), array('jquery'), time(), true );
}

function view_vertical_menu() {
  if ( has_nav_menu( 'Vertical_menu' ) ){
    wp_nav_menu( array(
      'theme_location'  => 'Vertical_menu',
      'container' => 'nav',
      'container_class' => 'vertical_menu_nav',
      'depth' => 2,
      ) );
    }
}