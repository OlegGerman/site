(function($) {
  $('nav.vertical_menu_nav > ul li.menu-item-has-children').click(function(event) {
    event.preventDefault();
    $(this).toggleClass('active-vertical-sub-menu');
    if($(this).hasClass('active-vertical-sub-menu')){
      $(this).children('ul').delay(200).slideDown(300); /* скорость появления пункта меню */
    } else {
      $(this).children('ul').delay(200).slideUp(200); /* скорость исчезновения пункта меню */
    }
  });  
})( jQuery );


